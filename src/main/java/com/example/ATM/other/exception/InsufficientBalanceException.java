package com.example.ATM.other.exception;

public class InsufficientBalanceException extends RuntimeException {

    public InsufficientBalanceException() {
        super("Insufficient balance");
    }
    public InsufficientBalanceException(String message) {
        super(message);
    }
}
