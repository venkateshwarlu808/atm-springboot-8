package com.example.ATM.view;

import com.example.ATM.model.dao.UserDAO;
import com.example.ATM.model.entity.User;
import com.example.ATM.utility.CharacterLengthValidator;
import com.example.ATM.utility.MaskFormatterHelper;
import com.example.ATM.controller.service.TransactionService;
import com.example.ATM.controller.service.UserService;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;
import javax.swing.text.MaskFormatter;

public class NewAccPage extends JFrame {
    private UserService userService;
    private TransactionService transactionService;
    private UserDAO userDAO;
    private JButton createAccountButton;
    private JButton cancelButton;
    private JPanel panel;
    private JTextField usernameField;
    private JFormattedTextField passwordField;
    private JTextField firstNameField;
    private JTextField lastNameField;

    public NewAccPage(TransactionService transactionService, UserService userService, UserDAO userDAO) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.userDAO = userDAO;


        createAccountButton = new JButton("Create New Account");
        cancelButton = new JButton("Cancel");
        panel = new JPanel(new GridLayout(5, 2, 10, 10));
        panel.setBackground(Color.PINK);

        usernameField = new JTextField(6);
        Optional<MaskFormatter> passwordFormatter = MaskFormatterHelper.createPasswordFormatter();
        passwordField = new JFormattedTextField(passwordFormatter.orElse(null));
        firstNameField = new JTextField(6);
        lastNameField = new JTextField(6);

        initComponents();
        addComponentsToPanel();
        addListeners();
        configureFrame();

    }

    private void initComponents() {
        createAccountButton.setForeground(Color.WHITE);
        createAccountButton.setBackground(Color.DARK_GRAY);
        createAccountButton.setEnabled(false);

        cancelButton.setForeground(Color.WHITE);
        cancelButton.setBackground(Color.DARK_GRAY);
    }
    private void addComponentsToPanel() {
        panel.add(new JLabel("Enter your username:"));
        panel.add(usernameField);
        panel.add(new JLabel("Enter your password: (7 numbers)"));
        panel.add(passwordField);
        panel.add(new JLabel("Enter your first name:"));
        panel.add(firstNameField);
        panel.add(new JLabel("Enter your last name:"));
        panel.add(lastNameField);
        panel.add(cancelButton);
        panel.add(createAccountButton);
    }



    private void addListeners() {
        createAccountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = passwordField.getText();
                String firstName = firstNameField.getText();
                String lastName = lastNameField.getText();
                double balance = 1000.0;

                User tempUser = new User(username, password, firstName, lastName, balance);
                userDAO.save(tempUser);
                System.out.println("A new user was inserted successfully!");

                dispose();
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        CharacterLengthValidator.createForUsername(createAccountButton, usernameField, 5, 16);
        CharacterLengthValidator.createForPassword(createAccountButton, passwordField, 7, 7);
        CharacterLengthValidator.createForFirstName(createAccountButton, firstNameField, 5, 16);
        CharacterLengthValidator.createForLastName(createAccountButton, lastNameField, 5, 18);
    }

    private void configureFrame() {
        add(panel, BorderLayout.CENTER);
        setSize(700, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Create New Account");
        pack();
    }
}
