package com.example.ATM.view;

import com.example.ATM.controller.service.TransactionService;
import com.example.ATM.model.dao.UserDAO;
import com.example.ATM.controller.service.UserService;
import com.example.ATM.model.entity.User;
import java.awt.*;
import java.awt.event.*;
import java.util.Optional;
import javax.swing.*;

public class WelcomePage extends JFrame {

    private TransactionService transactionService;
    private UserService userService;

    private UserDAO userDAO;
    private User user;

    private JButton transferButton;
    private JButton logoutButton;
    private JPanel panel;
    private JLabel idLabel;
    private JLabel balanceLabel;
    private JLabel nameLabel;
    private JLabel surnameLabel;
    private JLabel amountLabel;
    private JLabel toWhoLabel;
    private JTextField amountField;
    private JTextField toWhoField;

    public WelcomePage(User user, TransactionService transactionService, UserService userService, UserDAO userDAO) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.userDAO = userDAO;
        this.user = user;


        initComponents();
        addComponentsToPanel();
        addListeners();
        configureFrame();


    }

    private void initComponents() {
        transferButton = new JButton("Begin Transfer");
        logoutButton = new JButton("Log out");
        panel = new JPanel(new GridLayout(6, 4, 10, 10));
        idLabel = new JLabel();
        balanceLabel = new JLabel();
        nameLabel = new JLabel();
        surnameLabel = new JLabel();
        amountLabel = new JLabel();
        toWhoLabel = new JLabel();
        amountField = new JTextField();
        toWhoField = new JTextField();

        idLabel.setText("User Account Number: " + user.getId());
        balanceLabel.setText("Current User Balance: " + user.getBalance() + " zł");
        nameLabel.setText("First Name: " + user.getFirstName());
        surnameLabel.setText("Last Name: " + user.getLastName());
        amountLabel.setText("Desired amount to transfer: ");
        toWhoLabel.setText("ID of desired reciever: ");

        Font customFont = new Font(idLabel.getFont().getName(), Font.PLAIN, 18);
        idLabel.setFont(customFont);
        balanceLabel.setFont(customFont);
        nameLabel.setFont(customFont);
        surnameLabel.setFont(customFont);
        amountLabel.setFont(customFont);
        toWhoLabel.setFont(customFont);
    }
    private void addComponentsToPanel(){
        panel.add(idLabel);
        panel.add(balanceLabel);
        panel.add(nameLabel);
        panel.add(surnameLabel);
        panel.add(amountLabel);
        panel.add(amountField);
        panel.add(toWhoLabel);
        panel.add(toWhoField);
        panel.add(logoutButton);
        panel.add(transferButton);
        panel.setBackground(Color.CYAN);
    }

    private void addListeners() {
        transferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String amountText = amountField.getText();
                String recipientUserIdText = toWhoField.getText();

                try {
                    double enteredAmount = Double.parseDouble(amountText);
                    int recipientUserId = Integer.parseInt(recipientUserIdText);

                    if (userService.doesUserExist(user.getId()) && userService.doesUserExist(recipientUserId)) {
                        boolean transferSuccessful = transactionService.transferFunds(user.getId(), recipientUserId, enteredAmount);
                        if (transferSuccessful) {
                            Optional<User> updatedUser = userService.getUserById(user.getId());
                                if (updatedUser.isPresent()) {
                                    user = updatedUser.get();
                                    double newBalance = user.getBalance();
                                    balanceLabel.setText("Current User Balance: " + newBalance + " zł");
                                    JOptionPane.showMessageDialog(rootPane, "Money transferred successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                                    amountField.setText(null);
                                    toWhoField.setText(null);
                                } else {
                                    JOptionPane.showMessageDialog(rootPane, "That ID doesn't exist", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "Money transfer failed. Insufficient balance or wrong user ID.", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(rootPane, "Wrong user ID!", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(rootPane, "Invalid input. Please enter a valid number.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                LoginForm loginForm = new LoginForm(transactionService,userService , userDAO);
                loginForm.setLocationRelativeTo(null);
                loginForm.setVisible(true);
            }
        });

        toWhoField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (transferButton.isEnabled()) {
                    transferButton.doClick();
                }
            }
        });

        amountField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (transferButton.isEnabled()) {
                    transferButton.doClick();
                }
            }
        });
    }

    private void configureFrame() {
        getContentPane().add(new JLabel("Welcome " + user.getFirstName() + user.getLastName() + "!"), BorderLayout.NORTH);
        getContentPane().add(panel, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Welcome");
        setLocationRelativeTo(null);
        setSize(700, 400);
    }

}
