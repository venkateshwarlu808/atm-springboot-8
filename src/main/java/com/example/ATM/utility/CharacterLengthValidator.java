package com.example.ATM.utility;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CharacterLengthValidator {
    private int minLength;
    private int maxLength;
    private JButton targetButton;
    private JTextField textField;
    private boolean isTextValid;


    public CharacterLengthValidator(JButton targetButton, JTextField textField, int minLength, int maxLength) {
        this.targetButton = targetButton;
        this.textField = textField;
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.isTextValid = false;

        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkTextLength();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkTextLength();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkTextLength();
            }

            private void checkTextLength() {
                String text = textField.getText();
                isTextValid = (text.length() >= minLength && text.length() <= maxLength);
                enableTargetButton();

            }
        });

    }

    public boolean isTextValid() {
        return isTextValid;
    }

    private void enableTargetButton() {
        targetButton.setEnabled(isTextValid);
    }

    public static CharacterLengthValidator createForUsername(JButton targetButton, JTextField textField, int minLength, int maxLength) {
        return new CharacterLengthValidator(targetButton, textField, minLength, maxLength);
    }

    public static CharacterLengthValidator createForPassword(JButton targetButton, JFormattedTextField textField, int minLength, int maxLength) {
        return new CharacterLengthValidator(targetButton, textField, minLength, maxLength);
    }

    public static CharacterLengthValidator createForFirstName(JButton targetButton, JTextField textField, int minLength, int maxLength) {
        return new CharacterLengthValidator(targetButton, textField, minLength, maxLength);
    }

    public static CharacterLengthValidator createForLastName(JButton targetButton, JTextField textField, int minLength, int maxLength) {
        return new CharacterLengthValidator(targetButton, textField, minLength, maxLength);
    }
}
