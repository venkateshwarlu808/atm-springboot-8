package com.example.ATM.controller.service;

import com.example.ATM.controller.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserExistenceChecker {

    private UserRepository userRepository;

    @Autowired
    public UserExistenceChecker(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean doesUserExist(int userId) {
        return userRepository.existsById(userId);
    }
}
